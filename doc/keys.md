## Setting up the keys

### On your local machine

#### Google Maps API key

1.  Browse this [link](https://cloud.google.com/maps-platform/) to get your maps key.
2.  Choose CONSOLE option on the top right
3.  Create a new project from the console of your google account.
4.  Enable the Maps SDK for Android, Places SDK for Android/iOS, Maps JavaScript API and Places API for Web from API section and this will also generate an API key in credentials section.
5.  Copy the API key and paste that in .env file

#### FB Key

1.  Browse this [link](https://developers.facebook.com/) and login with your facebook account.
2.  Create a new app by choosing Add App under My Apps option.
3.  Get your App Id and App secret key from the top of your Dashboard.
4.  Browse this [link](https://console.firebase.google.com) and login with your gmail account, and then genreate an empty project.
5.  Go to Authentication tab, and under Sign in method enable Email/Password, Google and facebook method.
6.  While enabling facebook method, pass your App id and App secret key in given fields and copy the given redirected URI.

#### Firebase

Follow these steps to set up firebase in your app

1.  Go to your facebook developer project again and enter products sections, where you need to click on 'set up' facebook login.
2.  Now facebook login will start appearing in your products section, inside facebook login go to Quickstart.
3.  Inside Quickstart choose android/ios as your platform and complete the required information in Quickstart form.
4.  Now go to facebook login > settings and paste your copied redirected URI in Valid OAuth Redirect URIs field and save your changes.
5.  Now come back to your firebase console and go to settings > project settings, and choose android/ios platform.
6.  Fill all the fields in the directed form, and to get SHA1 and SHA256 fingerprint, run this command in your terminal, it's execution will ask for Keystore password, either you press Enter or type 'android'.

```
keytool -exportcert -list -v \
-alias androiddebugkey -keystore ~/.android/debug.keystore
```

7. Download the google-services.json file and add it to {PROJECT_NAME}/android/app.

Create a `.env` file in the root folder of the project and add your keys like the following:

```
FB_KEY=
GOOGLE_MAPS_KEY=
GOOGLE_WEB_CLIENT_ID=

IOS_GOOGLE_CLIENT_ID=
REVERSE_CLIENT_ID=
FB_CUSTOM_URL_SCHEME=
```

This will sync your keys with the project.

You will find these keys in the following files (Incase you want to modify anything related):

- `android/app/src/main/res/values/strings.xml`
- `src/components/login/homeLogin.js`
- `src/components/mapFeed/map/mapScreen.js`
- `src/actions/emergencyPlacesAction.js`

### **_Note:_**

- Don't forget to set Realtime Database rules as true in Database > rules.

- Setup your environment keys as stated above before you run `react-native run-android` or `react-native run-ios` or `any other build command`, otherwise your build will fail.

- Whenever you are setting up the keys for the first time ensure that when you download your `google-services.json` file and its in the same format with the `google-services.json.template` file. Otherwise change the `.template` file according to the latest file.
  **Contact the maintainer for further help and clarifications regarding any issue.**

### On your Forked repository for CI pipeline

Add the following keys in the secret variables section in the **Settings -> CI/CD -> Variables** of your forked repository to setup the pipeline keys.

```
FB_KEY=
GOOGLE_MAPS_KEY=
GOOGLE_WEB_CLIENT_ID=

IOS_GOOGLE_CLIENT_ID=
REVERSE_CLIENT_ID=
FB_CUSTOM_URL_SCHEME=

DOCKER_SHA1_KEY=
DOCKER_SHA1_KEY_CLIENT_ID=
API_KEY_GOOGLE_SERVICE=
GOOGLE_SERVICES_SERVICES_CLIENT_TYPE2=
```

![alt text](CI CD variables settings.png)

Rest of it will be handled by the CI itself. Just setup the keys as stated above in the _Secret variables_ section so that the CI can access those.
